#!/bin/bash

# Gather up all nodes on the current focused desktop
nodes=$(bspc query -N -d focused)

# Loop through the nodes, killing each one
for node in $nodes
do
	bspc node $node -k
done
