#!/bin/bash

# Assign the current default sink to the variable 'default'
default=$(pactl info | grep "Default Sink" | awk '{print $3}')

# I only ever want to use one of two different mics
# either the mic that is attached to my berhringer interface
# or the mic to the valve index headset, this is accomplished
# by simply calling 'pactl list shor sources' and grepping for a
# unique value and printing out the device name
indexMic=$(pactl list short sources | grep Valve | grep input | awk '{print $2}')
shureMic=$(pactl list short sources | grep CODEC | grep input | awk '{print $2}')

# Gather up all of the different audio outputs that have been enabled
allSinks=($(pactl list short sinks | awk '{print $2}'));

# Get the count of all of the sinks
sinkLength=$(expr ${#allSinks[@]} - 1);

# Initialize starting index variables to -1
desiredIndex=-1
matchingIndex=-1

# Loop through all of the different sinks
# If the currentSink in the loop is the same
# as the 'default' sink variable then check if
# the loop is at the last position. If it is 
# at the last position, the next sink we want to
# use will be back to 0. Otherwise add 1 to the current
# sink index, and set the 'desiredSink' variable to that
# value.
for i in ${!allSinks[@]};
do
	currentSink=${allSinks[$i]}
	if [ "$default" = "$currentSink" ];
	then
		if [ $i = $sinkLength ]
		then
			echo 'sink is in last position, use 0'
			desiredIndex=0
		else 
			echo 'use the next sink'
			desiredIndex=$(expr $i + 1)
		fi
		desiredSink=${allSinks[$desiredIndex]}
		break

	fi
done

# Checking which mic shoudl be active - if it is coming from the sink that
# controls the sound for the HMD of the valve index, then use that devices'
# micprohpone, otherwise just use the Shure SM7b
if [[ $desiredSink == *"hdmi"* ]];
then
	pactl set-default-source $indexMic
else
	pactl set-default-source $shureMic
fi
pactl set-default-sink $desiredSink

