#!/bin/bash
userName=skaiinyght
wallpaperDirectory=/home/$userName/pictures/wallpaper

# Use any image in the wallpaper directory regardless of nested folder depth
feh --bg-fill --randomize $wallpaperDirectory/* 
