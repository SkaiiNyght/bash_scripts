#!/bin/bash

# Connecting to nord vpn will always connect to a semi-random server, no need to disconnect
# and connect again, it will work by simply attempting to connect to a new server
nordvpn c
